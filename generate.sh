#!/usr/bin/env bash

ASSETS_PATH="$(pwd)/Assets"
NETWORK_PATH="${ASSETS_PATH}/Network"
PROTO_PATH="${NETWORK_PATH}/Protocol.proto"

protoc "${PROTO_PATH}" \
 --grpc_out="${NETWORK_PATH}" \
 --csharp_out="${NETWORK_PATH}" \
 --proto_path "${NETWORK_PATH}" \
 --proto_path "${NETWORK_PATH}" \
 --plugin=protoc-gen-grpc=/usr/local/bin/grpc_csharp_plugin

#--proto_path "/Users/mjensen/src/furgin/pong/Pong/Assets/ProtoBuf/Runtime/CustomOptions" \
