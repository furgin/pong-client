#!/usr/bin/env bash

. .buildrc

#set -ex

DOCKER_IMAGE="furgin/unity3d-pipe"

docker pull ${DOCKER_IMAGE}:latest

run_pipe_container() {
  dir="$(pwd)"
  docker container run -it \
    --volume=/usr/local/bin/docker:/usr/local/bin/docker:ro \
    --volume=/var/run/docker.sock:/var/run/docker.sock \
    --env=DRY_RUN="false" \
    --env=DEBUG="false" \
    --workdir=$dir \
    --volume=$dir:$dir \
    --env=BITBUCKET_CLONE_DIR="$dir" \
    --env=BITBUCKET_REPO_OWNER="furgin" \
    "$@" \
    ${DOCKER_IMAGE}:latest
}

action=${1:?'usage: build command [options]'}

case "${action}" in
    activation)
        UNITY_VERSION=${UNITY_VERSION:?'UNITY_VERSION variable missing.'}
        UNITY_USERNAME=${UNITY_USERNAME:?'UNITY_USERNAME variable missing.'}
        UNITY_PASSWORD=${UNITY_PASSWORD:?'UNITY_PASSWORD variable missing.'}
        run_pipe_container -eACTION=activation -eUNITY_VERSION=${UNITY_VERSION} -eUNITY_USERNAME=${UNITY_USERNAME} -eUNITY_PASSWORD=${UNITY_PASSWORD}
        ;;
    test)
        UNITY_VERSION=${UNITY_VERSION:?'UNITY_VERSION variable missing.'}
        TEST_PLATFORM=${2:?'usage: build test [editmore|playmode]'}
        UNITY_LICENSE_FILE=${UNITY_LICENSE_FILE:?'UNITY_LICENSE_FILE variable missing.'}
        run_pipe_container -eACTION=test -eUNITY_VERSION=${UNITY_VERSION} -eUNITY_LICENSE_CONTENT_BASE64=$(cat ${UNITY_LICENSE_FILE} | base64) -eTEST_PLATFORM=${TEST_PLATFORM}
        ;;
    build)
        UNITY_VERSION=${UNITY_VERSION:?'UNITY_VERSION variable missing.'}
        BUILD_NAME=${BUILD_NAME:?'BUILD_NAME variable missing.'}
        BUILD_TARGET=${2:?'usage: build test [StandaloneLinux64|StandaloneOSX|StandaloneWindows64|WebGL|Android|iOS]'}
        UNITY_LICENSE_FILE=${UNITY_LICENSE_FILE:?'UNITY_LICENSE_FILE variable missing.'}
        run_pipe_container -eACTION=build -eUNITY_VERSION=${UNITY_VERSION} -eUNITY_LICENSE_CONTENT_BASE64=$(cat ${UNITY_LICENSE_FILE} | base64) -eBUILD_NAME=${BUILD_NAME} -eBUILD_TARGET=${BUILD_TARGET}
        ;;
    dist)
        BUILD_NAME=${BUILD_NAME:?'BUILD_NAME variable missing.'}
        run_pipe_container -eACTION=dist -eBUILD_NAME=${BUILD_NAME}
        ;;
esac


