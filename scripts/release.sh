#!/usr/bin/env bash

bump_version () {
  previous_version=$1
  new_version=$2

  echo "Generating CHANGELOG.md file..."
  semversioner changelog > CHANGELOG.md

  echo "Replace version '$previous_version' to '$new_version' in README.md ..."
  sed -i "s/:$previous_version/:$new_version/g" README.md

}

git_push() {
  version="$1"
  branch="$2"
  tag="release-${version}"

  # Commit changes to readme etc to branch
  git add .changes README.md CHANGELOG.md
  git commit -m "Update files for new version '${version}' [skip ci]"
  git push origin master
  
  # Tag for current release
  git tag -a -m "Tagging for release ${version}" "${tag}"
  git push origin "${tag}"
  
  # Push release version to release branch to trigger deployment
  git pull origin release
  echo "public static class Version { public const string Current = \"${version}\"; }" > Assets/Version.cs
  echo "${version}" > version
  git add Assets/Version.cs version
  git commit -m "Update files for new version '${version}' on release branch"
  git push origin master:release
}

previous_version=$(semversioner current-version)
semversioner release
new_version=$(semversioner current-version)

branch=$(git rev-parse --abbrev-ref HEAD)

if [ "${previous_version}" != "${new_version}" ];
then
  set -ex
  bump_version "$previous_version" "$new_version"
  git_push "$new_version" "$branch"
fi



