﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GameHUD : MonoBehaviour
{
    public int Player1Score
    {
        get => int.Parse(_scores[0].text);
        set => _scores[0].text = value.ToString();
    }
    public int Player2Score
    {
        get => int.Parse(_scores[1].text);
        set => _scores[1].text = value.ToString();
    }

    private Text[] _scores;
    private Text[] _names;

    private void Start()
    {
        _scores = new Text[2];
        _scores[0] = transform.FindAll("Player1 Score").GetComponent<Text>();
        _scores[1] = transform.FindAll("Player2 Score").GetComponent<Text>();
        _names = new Text[2];
        _names[0] = transform.FindAll("Player1 Name").GetComponent<Text>();
        _names[1] = transform.FindAll("Player2 Name").GetComponent<Text>();
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void AddScore(int player, int score)
    {
        SetScore(player, int.Parse(_scores[player].text) + score);
    }

    public void SetScore(int player, int score)
    {
        _scores[player].text = score.ToString("00");
    }

    public void SetName(int player, string playerName)
    {
        _names[player].text = playerName.ToUpper();
    }

    public void Reset()
    {
        SetScore(0, 0);
        SetScore(1, 0);
        SetName(0, "");
        SetName(1, "");
    }

    public int Player(string playerName)
    {
        return _names.Select(t => t.text).ToList().IndexOf(playerName);
    }
}