﻿using UnityEngine;
using UnityEngine.UI;

public class Spinner : MonoBehaviour
{
    private Text _spinnerText;
    private float _count;
    private int _value;
    private string _characters = "0123456789AB";
    public float Speed = 4f;
    
    public void Show()
    {
        if(gameObject != null)
            gameObject.SetActive(true);
    }

    public void Hide()
    {
        if(gameObject != null)
            gameObject.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
        _spinnerText = transform.FindAll("SpinnerText").GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        _count += Time.deltaTime * Speed;
        while (_count > 1f)
        {
            _value = ((_value + 1) % _characters.Length);
            _spinnerText.text = _characters[_value].ToString();
            _count--;
        }
    }
}