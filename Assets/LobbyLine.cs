﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LobbyLine : MonoBehaviour
    , IPointerClickHandler
    , IPointerEnterHandler
    , IPointerExitHandler
{
    public Lobby Lobby;

    public Color HoverColor;
    private Color _defaultColor;
    private Text _text;
    private Text _status;
    private Image _image;
    private Color _target;

// Start is called before the first frame update
    void Start()
    {
        _defaultColor = Image.color;
    }

    public Image Image
    {
        get
        {
            if (_image != null)
            {
                return _image;
            }
            return _image = GetComponent<Image>();
        }
    }

    public Text Text
    {
        get
        {
            if (_text != null)
            {
                return _text;
            }

            return _text = transform.FindAll("Text").GetComponent<Text>();
        }
    }

    public Text Status
    {
        get
        {
            if (_status != null)
            {
                return _status;
            }
            return _status = transform.FindAll("Status").GetComponent<Text>();
        }
    }
    
    public void Clear()
    {
        Text.text = "";
        Status.text = "";
    }

    public void PlayerName(string playerName)
    {
        Text.text = playerName.ToUpper();
    }
    public void OpponentName(string opponentName)
    {
        Status.text = opponentName.ToUpper();
    }

    public void OnPointerClick(PointerEventData eventData) // 3
    {
        if (eventData.clickCount == 2)
        {
            Lobby.DoubleClick(Text.text);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (_text.text != "")
        {
            _target = HoverColor;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        _target = _defaultColor;
    }
    
    void Update()
    {
        if (_image)
            _image.color = _target;
    }
}