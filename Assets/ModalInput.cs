﻿using UnityEngine;
using UnityEngine.UI;

public class ModalInput : MonoBehaviour
{
    public void Show(string prompt)
    {
        gameObject.SetActive(true);
        gameObject.transform.FindAll("Prompt").GetComponent<Text>().text = prompt;
        gameObject.transform.FindAll("PromptInputField").GetComponent<InputField>().ActivateInputField();
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}