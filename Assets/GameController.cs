﻿using Pong;
using UnityEditor;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public Game Game;
    public Message Message;
    public ModalInput ModalInput;
    public Spinner Spinner;
    public Lobby Lobby;
    public GameHUD GameHUD;
    public Ball Ball;

    private State _state;
    private Client _client;
    private string _player1;
    private string _player2;
    public string CurrentPlayer { get; private set; }

    private enum State
    {
        LoginPrompt,
        Connecting,
        Playing,
        Lobby,
        Spectating
    }

    // Start is called before the first frame update
    private void Start()
    {
        Application.runInBackground = true;
        _client = GetComponent<Client>();
        EndGame();
        LoginPrompt();
    }

    private void LoginPrompt()
    {
        Game.Hide();
        _state = State.LoginPrompt;
        ModalInput.Show("NAME");
    }

    private void Connect(string playerName)
    {
        Lobby.Clear();
        _state = State.Connecting;
        Spinner.Show();
        Message.Hide();
        CurrentPlayer = playerName;
        _client.Connect(playerName);
    }

    public void OnModalSubmit(string value)
    {
        if (_state == State.LoginPrompt)
        {
            ModalInput.Hide();
            Connect(value);
        }
    }

    public void OnConnectFailed(string error)
    {
        Message.Show(error);
        Spinner.Show();
        Lobby.Hide();
        LoginPrompt();
    }

    private void GameLobby()
    {
        GameHUD.Hide();
        Lobby.Show();
    }

    public void OnPlayerConnect(string playerName)
    {
        if (playerName.Equals(CurrentPlayer))
        {
            _state = State.Lobby;
            Message.Hide();
            Spinner.Hide();
            GameLobby();
        }

        Lobby.AddPlayer(playerName);
    }

    public void OnPlayerDisconnect(string playerName)
    {
        Lobby.RemovePlayer(playerName);
        // if on of the current players disconnects, end the game
        if (_state == State.Playing && (playerName.Equals(_player1) || playerName.Equals(_player2)))
        {
            EndGame();
            GameLobby();
            Message.Show(playerName.ToUpper() + " DISCONNECTED");
        }
    }

    public void OnAccept(string player1, string player2)
    {
        StartGame(player1, player2);
    }

    private void StartGame(string player1, string player2)
    {
        _state = State.Playing;
        Message.Hide();
        Lobby.Hide();
        Game.Show();
        GameHUD.Show();
        GameHUD.Reset();
        GameHUD.SetName(0, player1);
        GameHUD.SetName(1, player2);
        Game.StartGame();
        _player1 = player1;
        _player2 = player2;
    }

    private void EndGame()
    {
        _player1 = null;
        _player2 = null;
        _state = State.Lobby;
        Game.Reset();
        Game.Hide();
    }

    public void OnEndGame(EndGame endGame)
    {
        if ((_state == State.Playing || _state == State.Spectating) &&
            (_player1 != null && _player2 != null) &&
            (_player1.Equals(endGame.Player1) || _player2.Equals(endGame.Player2)))
        {
            Lobby.EndGame(endGame);
            Message.Show(endGame.Message);
            EndGame();
            GameLobby();
        }
        else
        {
            Lobby.EndGame(endGame);
        }
    }

    private void Update()
    {
        if (_state == State.Playing)
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                _client.SendMove(1);
            }

            if (Input.GetKeyUp(KeyCode.UpArrow))
            {
                _client.SendMove(0);
            }

            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                _client.SendMove(-1);
            }

            if (Input.GetKeyUp(KeyCode.DownArrow))
            {
                _client.SendMove(0);
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                _client.SendEndGame(_player1, _player2, CurrentPlayer + " QUIT");
                EndGame();
                GameLobby();
                Message.Show("QUIT GAME");
            }
        }
        else if (_state == State.Spectating)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                EndGame();
                GameLobby();
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
#if UNITY_EDITOR
                EditorApplication.isPlaying = false;
#else
         Application.Quit();
#endif
            }
        }
    }

    public void ChallengePlayer(string playerName)
    {
        if (!playerName.Equals(CurrentPlayer))
        {
            _client.SendChallenge(playerName);
        }
    }

    public void SpectatePlayer(string playerName)
    {
        if (!playerName.Equals(CurrentPlayer))
        {
            _client.SendSpectate(playerName);
        }
    }

    public void OnGameState(GameState gameState)
    {
        var ballPosition = gameState.BallPosition;
        var ballVelocity = gameState.BallVelocity;
        Ball.SetBallState(new Vector2(ballPosition.X, ballPosition.Y), new Vector2(ballVelocity.X, ballVelocity.Y));
        Game.UpdatePlayer(0,
            new Vector3(gameState.Player1.Position.X, gameState.Player1.Position.Y, 0),
            new Vector3(gameState.Player1.Velocity.X, gameState.Player1.Velocity.Y, 0)
        );
        Game.UpdatePlayer(1,
            new Vector3(gameState.Player2.Position.X, gameState.Player2.Position.Y, 0),
            new Vector3(gameState.Player2.Velocity.X, gameState.Player2.Velocity.Y, 0)
        );
        GameHUD.Player1Score = gameState.Player1.Score;
        GameHUD.Player2Score = gameState.Player2.Score;
    }

    public void OnError(Error error)
    {
        print(error);
        print(_state);
        switch (_state)
        {
            case State.Connecting:
                Message.Show(error.Message);
                Spinner.Hide();
                LoginPrompt();
                break;
            case State.Lobby:
                Message.Show(error.Message);
                break;
        }
    }

    public void OnNewGame(NewGame newGame)
    {
        // update the lobby to show in progress games
        Lobby.NewGame(newGame);
    }

    public void OnCountdown(Countdown countdown)
    {
        Message.Show(countdown.Countdown_ == 0 ? "GO!" : countdown.Countdown_.ToString());
    }

    public void OnSpectateAccept(SpectateAccept spectateAccept)
    {
        if (_state == State.Lobby)
        {
            _state = State.Spectating;
            StartGame(spectateAccept.Player1, spectateAccept.Player2);
            Message.Show("SPECTATING");
        }
    }

    public void OnChallenge(ChallengeRequest challenge)
    {
        if (_state == State.Lobby)
        {
            _client.SendGameAccept(challenge.Player1, challenge.Player2);
            StartGame(challenge.Player1, challenge.Player2);
        }
    }
}