﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using UnityEngine;
using Grpc.Core;
using Grpc.Core.Utils;
using Pong;

public class Client : MonoBehaviour
{
    private GameController _gameManager;
    private PongService.PongServiceClient _client;
    private AsyncDuplexStreamingCall<Pong.Message, Pong.Message> _stream;
    private Task _connectTask;
    private Task<bool> _readTask;
    private Task _writeTask;
    private Channel _channel;
    private bool _connected;
    private List<Pong.Message> _writeBuffer = new List<Pong.Message>();

    // Start is called before the first frame update
    void Start()
    {
        _channel = new Channel(
//#if UNITY_EDITOR
//            "localhost:50051",
//#else
            "pong.furgin.org:50051",
//#endif
            ChannelCredentials.Insecure);
        _gameManager = GetComponent<GameController>();
        _connected = false;
    }

    public void Connect(string playerName)
    {
        if (!_connected)
        {
            print("Connect: " + playerName);
            _connectTask = _channel.ConnectAsync(DateTime.UtcNow.AddSeconds(5));
        }

        var message = new Pong.Message {Connected = new PlayerConnected {Name = playerName}};
        _writeBuffer.Add(message);
    }

    // Update is called once per frame
    void Update()
    {
        if (_connected)
        {
            ProcessReadStream();
            ProcessWriteStream();
        }
        else
        {
            // trying to connect
            if (_connectTask != null && _connectTask.IsCompleted)
            {
                print("Trying...");
                var status = _connectTask.Status;
                switch (status)
                {
                    case TaskStatus.Canceled:
                    case TaskStatus.Faulted:
                        _connectTask.Dispose();
                        _gameManager.OnConnectFailed("SERVER DOWN");
                        break;
                    case TaskStatus.RanToCompletion:

                        _client = new PongService.PongServiceClient(_channel);
                        Debug.Log("Connected: " + _channel.State);
                        _stream = _client.Connect();
                        _connected = true;
                        break;
                }

                _connectTask = null;
            }
        }
    }

    private void ProcessReadStream()
    {
        if (_readTask == null)
        {
            _readTask = _stream.ResponseStream.MoveNext();
        }

        if (_readTask != null && _readTask.IsCompleted)
        {
            var status = _readTask.Status;
            switch (status)
            {
                case TaskStatus.Faulted:
                    Disconnected();
                    break;
                case TaskStatus.RanToCompletion:
                    if (_readTask.Result)
                    {
                        var message = _stream.ResponseStream.Current;
                        if (message.PayloadCase != Pong.Message.PayloadOneofCase.GameState) Debug.Log(message);
                        switch (message.PayloadCase)
                        {
                            case Pong.Message.PayloadOneofCase.Connected:
                                _gameManager.OnPlayerConnect(message.Connected.Name);
                                break;
                            case Pong.Message.PayloadOneofCase.Disconnected:
                                _gameManager.OnPlayerDisconnect(message.Disconnected.Name);
                                break;
                            case Pong.Message.PayloadOneofCase.Accept:
                                _gameManager.OnAccept(message.Accept.Player1, message.Accept.Player2);
                                break;
                            case Pong.Message.PayloadOneofCase.Challenge:
                                _gameManager.OnChallenge(message.Challenge);
                                break;
                            case Pong.Message.PayloadOneofCase.EndGame:
                                var endGame = message.EndGame;
                                _gameManager.OnEndGame(endGame);
                                break;
                            case Pong.Message.PayloadOneofCase.GameState:
                                var gameState = message.GameState;
                                _gameManager.OnGameState(gameState);
                                break;
                            case Pong.Message.PayloadOneofCase.NewGame:
                                var newGame = message.NewGame;
                                _gameManager.OnNewGame(newGame);
                                break;
                            case Pong.Message.PayloadOneofCase.Error:
                                _gameManager.OnError(message.Error);
                                break;
                            case Pong.Message.PayloadOneofCase.Countdown:
                                _gameManager.OnCountdown(message.Countdown);
                                break;
                            case Pong.Message.PayloadOneofCase.SpectateAccept:
                                _gameManager.OnSpectateAccept(message.SpectateAccept);
                                break;
                        }
                    }

                    break;
            }

            _readTask = null;
        }
    }

    private void ProcessWriteStream()
    {
        if (_writeTask == null && _writeBuffer.Count > 0)
        {
            _writeTask = _stream.RequestStream.WriteAllAsync(_writeBuffer, complete: false);
        }

        if (_writeTask != null && _writeTask.IsCompleted)
        {
            var status = _writeTask.Status;
            switch (status)
            {
                case TaskStatus.Faulted:
                    print(_writeTask.Exception);
                    Disconnected();
                    break;
                case TaskStatus.RanToCompletion:
                    _writeBuffer.Clear();
                    break;
            }

            _writeTask = null;
        }
    }

    void Disconnected()
    {
        _readTask?.Dispose();
        _writeTask?.Dispose();
        _stream?.Dispose();

        _connectTask = null;
        _readTask = null;
        _writeTask = null;
        _stream = null;
        _connected = false;
        _gameManager.OnConnectFailed("disconnected");
        _writeBuffer.Clear();
    }

    void OnDisable()
    {
        _stream?.Dispose();
    }

    public void SendChallenge(string playerName)
    {
        _writeBuffer.Add(new Pong.Message
            {Challenge = new ChallengeRequest {Player1 = _gameManager.CurrentPlayer, Player2 = playerName}});
    }

    public void SendSpectate(string playerName)
    {
        _writeBuffer.Add(new Pong.Message
            {SpectateRequest = new SpectateRequest {Player = playerName}});
    }

    public void SendMove(float direction)
    {
        _writeBuffer.Add(new Pong.Message
            {Move = new Move {Direction = direction}});
    }

    public void SendGameAccept(string player1, string player2)
    {
        _writeBuffer.Add(new Pong.Message
        {
            Accept = new ChallengeAccept
            {
                Player1 = player1,
                Player2 = player2
            }
        });
    }

    public void SendEndGame(string player1, string player2, string message)
    {
        _writeBuffer.Add(new Pong.Message
        {
            EndGame = new EndGame()
            {
                Player1 = player1,
                Player2 = player2,
                Message = message
            }
        });
    }
}