﻿using System;
using UnityEngine;

public class Ball : MonoBehaviour
{
    private Rigidbody2D _rigidBody;

    private void Start()
    {
        _rigidBody = GetComponent<Rigidbody2D>();
    }

    public void SetBallState(Vector2 ballPosition, Vector2 ballVelocity)
    {
        _rigidBody.position = ballPosition;
        _rigidBody.velocity = ballVelocity;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        print(other.collider.name+" collision at "+transform.position);
    }
}