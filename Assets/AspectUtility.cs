﻿using System;
using UnityEngine;

[ExecuteAlways]
public class AspectUtility : MonoBehaviour
{
    public float DesiredWidth = 16;
    public float DesiredHeight = 9;
    private float _desiredAspectRatio;
    private Camera _camera;
    private Camera _backgroundCamera;

    private int _screenWidth = -1;
    private int _screenHeight = -1;

    private void OnValidate()
    {
        Awake();
    }

    private void Awake()
    {
        _camera = GetComponent<Camera>();
        if (!_camera)
        {
            _camera = Camera.main;
        }

        if (!_camera)
        {
            Debug.LogError("No camera available");
            return;
        }

        var backgroundCameraObject = GameObject.Find("BackgroundCamera");
        if (!backgroundCameraObject)
        {
            _backgroundCamera = new GameObject("BackgroundCamera", typeof(Camera)).GetComponent<Camera>();
            _backgroundCamera.depth = int.MinValue;
            _backgroundCamera.clearFlags = CameraClearFlags.SolidColor;
            _backgroundCamera.backgroundColor = Color.black;
            _backgroundCamera.cullingMask = 0;
        }
        else
        {
            _backgroundCamera = backgroundCameraObject.GetComponent<Camera>();
        }

        _desiredAspectRatio = DesiredWidth / DesiredHeight;
        SetCamera();
    }

    private void Update()
    {
        if (_screenHeight != Screen.height || _screenWidth != Screen.width)
        {
            _screenHeight = Screen.height;
            _screenWidth = Screen.width;
            SetCamera();
        }
    }

    private void SetCamera()
    {
        var currentAspectRatio = (float) Screen.width / Screen.height;
        // If the current aspect ratio is already approximately equal to the desired aspect ratio,
        // use a full-screen Rect (in case it was set to something else previously)
        if (Math.Abs((int) (currentAspectRatio * 100f) / 100f - (int) (_desiredAspectRatio * 100f) / 100f) <
            Mathf.Epsilon)
        {
            _camera.rect = new Rect(0.0f, 0.0f, 1.0f, 1.0f);
            return;
        }

        // Pillarbox
        if (currentAspectRatio > _desiredAspectRatio)
        {
            var inset = 1.0f - _desiredAspectRatio / currentAspectRatio;
            _camera.rect = new Rect(inset / 2, 0.0f, 1.0f - inset, 1.0f);
        }
        // Letterbox
        else
        {
            var inset = 1.0f - currentAspectRatio / _desiredAspectRatio;
            _camera.rect = new Rect(0.0f, inset / 2, 1.0f, 1.0f - inset);
        }
    }
}