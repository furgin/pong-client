using System.Collections.Generic;
using System.Linq;
using Pong;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

class PlayerDetails
{
    public string Name { get; internal set; }
    public string Opponent { get; internal set; }
}

public class Lobby : MonoBehaviour
{
    public GameController GameController;

    private List<PlayerDetails> _players = new List<PlayerDetails>();
    private List<LobbyLine> _lines;

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    private void Start()
    {
        CreateLines();
        UpdateList();
    }

    private void CreateLines()
    {
        if (_lines == null)
        {
            var line1Object = transform.FindAll("Line1").gameObject;

            _lines = new List<LobbyLine>();
            _lines.Add(line1Object.GetComponent<LobbyLine>());
            var line1Position = line1Object.transform.localPosition;
            for (var i = 1; i < 7; i++)
            {
                var lineObject = Instantiate(line1Object, line1Object.transform.parent);
                lineObject.transform.localPosition = new Vector3(
                    line1Position.x,
                    line1Position.y - i * 40,
                    line1Position.z
                );
                _lines.Add(lineObject.GetComponent<LobbyLine>());
            }
        }
    }

    private void UpdateList()
    {
        CreateLines();
        foreach (var line in _lines)
        {
            line.Clear();
        }

        for (var i = 0; i < _lines.Count && i < _players.Count; i++)
        {
            var details = _players[i];
            _lines[i].PlayerName(details.Name);
            if (details.Opponent != null)
            {
                _lines[i].OpponentName(details.Opponent);
            }

//            if (_players[i].Name == GameController.CurrentPlayer)
//            {
//                _lines[i].OpponentName("You");
//            }
        }
    }

    public void Clear()
    {
        _players.Clear();
    }

    public void AddPlayer(string player)
    {
        _players.Add(new PlayerDetails() {Name = player});
        UpdateList();
    }

    public void RemovePlayer(string player)
    {
        _players = _players.Where(p => player != p.Name).ToList();
        UpdateList();
    }

    public void NewGame(NewGame newGame)
    {
        _players.ForEach(p =>
        {
            if (p.Name.Equals(newGame.Player1))
            {
                p.Opponent = newGame.Player2;
            }

            if (p.Name.Equals(newGame.Player2))
            {
                p.Opponent = newGame.Player1;
            }
        });
        UpdateList();
    }

    public void EndGame(EndGame endGame)
    {
        _players.ForEach(p =>
        {
            if (p.Name.Equals(endGame.Player1))
            {
                p.Opponent = null;
            }

            if (p.Name.Equals(endGame.Player2))
            {
                p.Opponent = null;
            }
        });
        UpdateList();
    }

    public void DoubleClick(string player)
    {
        var playerDetails = _players.Find(p => p.Name.Equals(player));
        if (playerDetails.Opponent!=null)
        {
            GameController.SpectatePlayer(player);
        }
        else
        {
            GameController.ChallengePlayer(player);
        }
    }
}