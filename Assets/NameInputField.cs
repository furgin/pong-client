﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class NameInputField : MonoBehaviour
{
    public GameController GameController;
    private InputField _field;

    // Start is called before the first frame update
    void Start()
    {
        _field = GetComponent<InputField>();
        _field.onValueChanged.AddListener(delegate { OnValueChanged();});
        _field.onEndEdit.AddListener(delegate { OnSubmit();});
    }

    void OnValueChanged()
    {
        const string valid = "ABCDEFGHIJKLMNOPQRSTUVWXYZ 123456789";
        var text = _field.text.ToUpper();
        var fixedText = text.Where(c => valid.Contains(c.ToString())).Aggregate("", (current, c) => current + c);
        _field.text = fixedText;
    }

    void OnSubmit()
    {
        GameController.OnModalSubmit(_field.text);
    }
}
