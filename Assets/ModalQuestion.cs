﻿using UnityEngine;
using UnityEngine.UI;

public class ModalQuestion : MonoBehaviour
{
    public void Show(string prompt)
    {
        gameObject.SetActive(true);
        gameObject.transform.FindAll("Prompt").GetComponent<Text>().text = prompt;
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
