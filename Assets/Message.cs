﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEngine;
using UnityEngine.UI;

public class Message : MonoBehaviour
{
    private float _messageTime = 0;
    private Text _message;

    // Start is called before the first frame update
    void Start()
    {
        _message = transform.FindAll("Message").GetComponent<Text>();
    }

    public void Show(string message, float time = 2f)
    {
        _messageTime = Time.time + time;
        _message.text = message.ToUpper();
    }

    public void Hide()
    {
        _message.text = "";
    }

    public void Update()
    {
        if (_messageTime > 0 && Time.time > _messageTime)
        {
            _messageTime = 0f;
            Hide();
        }
    }
}