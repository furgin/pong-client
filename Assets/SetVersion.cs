﻿using UnityEngine;
using UnityEngine.UI;

public class SetVersion : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Text>().text = Version.Current.ToUpper();
    }
}
