using System.Collections.Generic;
using UnityEngine;

public static class TransformExtensions
{
    //Breadth-first search
    public static Transform FindAll(this Transform aParent, string aName)
    {
        var queue = new Queue<Transform>();
        queue.Enqueue(aParent);
        while (queue.Count > 0)
        {
            var c = queue.Dequeue();
            if (c.name == aName)
                return c;
            foreach (Transform t in c)
                queue.Enqueue(t);
        }

        return null;
    }
}