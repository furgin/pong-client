﻿using UnityEngine;

public class ScreenBorders : MonoBehaviour
{
    public Camera Camera;
    public float ColorDepth = 4f;

    private Transform _top;
    private Transform _bottom;
    private Transform _left;
    private Transform _right;

    private Vector2 _screenSize;
    public float TopPosition { get; private set; }
    public float BottomPosition { get; private set; }

    // Start is called before the first frame update
    private void Start()
    {
        //Generate our empty objects
        _top = new GameObject().transform;
        _bottom = new GameObject().transform;
        _right = new GameObject().transform;
        _left = new GameObject().transform;

        //Name our objects 
        _top.name = "Top";
        _bottom.name = "Bottom";
        _right.name = "Right";
        _left.name = "Left";

        //Add the colliders
        _top.gameObject.AddComponent<BoxCollider2D>();
        _bottom.gameObject.AddComponent<BoxCollider2D>();
        _right.gameObject.AddComponent<BoxCollider2D>();
        _left.gameObject.AddComponent<BoxCollider2D>();

        var parent = transform;

        //Make them the child of whatever object this script is on, preferably on the Camera so the objects move with the camera without extra scripting
        _top.parent = parent;
        _bottom.parent = parent;
        _right.parent = parent;
        _left.parent = parent;

        //Generate world space point information for position and scale calculations
        _screenSize.x = Vector2.Distance(Camera.ScreenToWorldPoint(new Vector2(0, 0)),
                            Camera.ScreenToWorldPoint(new Vector2(Screen.width, 0))) * 0.5f;
        _screenSize.y = Vector2.Distance(Camera.ScreenToWorldPoint(new Vector2(0, 0)),
                            Camera.ScreenToWorldPoint(new Vector2(0, Screen.height))) * 0.5f;

        var position = Camera.transform.position;
        _right.localScale = new Vector3(ColorDepth, _screenSize.y * 2, ColorDepth);
        _right.position = new Vector3(position.x + _screenSize.x + (_right.localScale.x * 0.5f), position.y, 0);
        _left.localScale = new Vector3(ColorDepth, _screenSize.y * 2, ColorDepth);
        _left.position = new Vector3(position.x - _screenSize.x - (_left.localScale.x * 0.5f), position.y, 0);
        _top.localScale = new Vector3(_screenSize.x * 2, ColorDepth, ColorDepth);
        TopPosition = position.y + _screenSize.y + (_top.localScale.y * 0.5f);
        _top.position = new Vector3(position.x, TopPosition, 0);
        _bottom.localScale = new Vector3(_screenSize.x * 2, ColorDepth, ColorDepth);
        BottomPosition = position.y - _screenSize.y - (_bottom.localScale.y * 0.5f);
        _bottom.position = new Vector3(position.x, BottomPosition, 0);
    }

}