﻿using UnityEngine;

public class Game : MonoBehaviour
{
    public ScreenBorders ScreenBorders;

    private Transform[] _players;
    private GameObject _ballObject;
    private Ball _ball;

    // Start is called before the first frame update
    void Start()
    {
        _players = new Transform[2];
        _players[0] = transform.FindAll("Player1");
        _players[1] = transform.FindAll("Player2");
        _ballObject = transform.FindAll("Ball").gameObject;
        _ball = _ballObject.GetComponent<Ball>();
    }

    public void UpdatePlayer(int player, Vector3 position, Vector3 velocity)
    {
        _players[player].position = position;
        var rigidBody = _players[player].GetComponent<Rigidbody2D>();
        rigidBody.velocity = velocity;
        rigidBody.angularVelocity = 0f;
    }

    public void Reset()
    {
        _ball.transform.position = Vector3.zero;
        var rigidBody = _ball.GetComponent<Rigidbody2D>();
        rigidBody.velocity = Vector3.zero;
        rigidBody.angularVelocity = 0f;
    }

    public void StartGame()
    {
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }
}