# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.1.2

- patch: Fixed state check for challenge

## 0.1.1

- patch: Accept game challenge only when in lobby

## 0.1.0

- minor: Spectator Mode

## 0.0.9

- patch: User pipe for dists

## 0.0.8

- patch: Add version to game screen

## 0.0.7

- patch: Alternate serve direction
- patch: Use physics to move paddles

## 0.0.6

- patch: Countdown before serving
- patch: End game at 3 points
- patch: Reset ball after scoring
- patch: Score tracking
- patch: Speed up ball slowly

## 0.0.5

- patch: force aspect ratio

## 0.0.4

- patch: Add opponent names to player lobby list
- patch: Prevent challenge for players in game
- patch: Prevent invalid player names

## 0.0.3

- patch: Server and endgame

## 0.0.2

- patch: Paddle collision detection
- patch: Paddle movement
- patch: Paddle rebound control
- patch: Player disconnection improvements

## 0.0.1

- patch: Initial Release

